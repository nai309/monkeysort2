#ifndef IMAGE_WINDOW_HPP
#define IMAGE_WINDOW_HPP
#include <windowing/windows.hpp>
#include <windowing/win_types.hpp>
#include <windowing/imlib.hpp>
#include "filelist.hpp"
#include "destlist.hpp"
#include "general_settings.hpp"

#define PRGM_NAME PACKAGE_NAME
#define SYS_DIR_LOCATION  "/usr/share/" PRGM_NAME  "/"
#define DEF_CONF_NAME PRGM_NAME ".conf"
#define DEF_CONF_LOCATION SYSCONFDIR "/" DEF_CONF_NAME
#define USER_DIR_LOCATION ".config/" PRGM_NAME "/"
#define CACHE_DIR_LOCATION ".cache/" PRGM_NAME "/"
#define USER_CONF_LOCATION USER_DIR_LOCATION DEF_CONF_NAME

#define ctlm [this](cont::arg_list & a)
#define clm [](cont::arg_list & a)

enum text_type {
  DEST,
  POSITION,
  NONE
};

#ifdef VID
namespace video
{
  static const std::vector<std::string> extensions = {
    ".mp4",
    ".webm",
    ".mkv",
    ".avi"
  };
    
  bool check_extension(const std::string & filepath);

  std::string find_preview(const std::string & filepath);
}
#endif

/**
 * Class defining the main image window of the program.
 */
class image_window : public imlib::imlib_window,
		     public win::multifunction_win
{
private:
  //! Default image zoom level.
  static constexpr double default_zoom = 1;
  //! Factor that the zoom level is multiplied by at each step.
  static constexpr double zoom_constant = 1.21;
  double zoom_factor = default_zoom;
  dests::dest_group & dest_lists;
  int current_dest_list = -1;
  bool show_dest_list = true;
  unsigned int copy_mode = 0;
  files::filelist & images;
  std::list<std::string> dest_hist, pos_hist, null_hist;
  unsigned int pos = 0;
  struct
  {
    bool enabled = false;
    unsigned int start = 0;
  } selection_state;
  bool drag = false;
  util::point drag_point = {0,0}, pointer;
  
  void image_drag_set(XButtonEvent & xbutton, bool state)
    {
      drag = state;
      if (drag)
	{
	  pointer = util::point(xbutton.x_root,xbutton.y_root);
	}
    }
  void zoom_in() { zoom_factor *= zoom_constant; }
  void zoom_out() { if (zoom_factor > 0.01) zoom_factor /= zoom_constant; }
  void reset_zoom() { zoom_factor = default_zoom; }

  bool index(const int i);

  void generate_title();

  void cycle(const int i);

  void cycle_if_removed(const int i);

  void seek(const unsigned int i);

  void cycle_destlist(const int i);
  
  std::vector<std::string> format_dests(dests::destlist & d);

  std::string text_prompt(const std::string & title, text_type t);

  void selection_toggle();

  void move(const std::string & dest);

  void rename(const std::string & newname, bool include_ext = false);

  void remove();

  void add_dest();

  void change_dest();

  void remove_dest();

  void toggle_dest_list_show()
  {
    if (show_dest_list == true)
      show_dest_list = false;
    else
      show_dest_list = true;

    set_exposed();
  }

  void toggle_copy_mode(const unsigned int c)
  {
    if (copy_mode == 0)
      {
	copy_mode = c;
      }
    else
      {
	copy_mode = 0;
      }
    generate_title();
  }

#ifdef VID
  void play_video(const int i);
#endif

  void create_new_dir(const std::string & dir);

public:
  cont::cvect def_controls =
  {
    { "cycle_backward", ctlm {this->cycle(-1); return 0;}, XK_Up, win::MOD_NONE },
    { "cycle_forward", ctlm {this->cycle(1); return 0;}, {{XK_Down, win::MOD_NONE},{XK_KP_Enter, win::MOD_NONE}} },
    { "zoom_in", ctlm {this->zoom_in(); this->set_exposed(); return 0;}, XK_Up, win::MOD_SHIFT },
    { "zoom_out", ctlm {this->zoom_out(); this->set_exposed(); return 0;}, XK_Down, win::MOD_SHIFT },
    { "move_destlist_left", ctlm {this->cycle_destlist(-1); return 0;}, XK_Left, win::MOD_NONE },
    { "move_destlist_right", ctlm {this->cycle_destlist(1); return 0;}, XK_Right, win::MOD_NONE },
    { "toggle_destlist_show", ctlm {this->toggle_dest_list_show(); return 0;}, XK_l, win::MOD_NONE },
    { "undo", ctlm {this->images.undo(); return 0;}, XK_u, win::MOD_CTRL },
    { "delete", ctlm {std::string text_return = this->text_prompt("confirm deletion[y/N]", text_type::DEST); if (text_return == "y") this->remove(); return 0;}, XK_d, win::MOD_CTRL },
    { "enter", ctlm {std::string text_return = this->text_prompt("enter destination", text_type::DEST); if (text_return.size() > 0) move(text_return); return 0;}, XK_Return, win::MOD_NONE },
    { "rename", ctlm {std::string text_return = this->text_prompt("rename", text_type::DEST); if (text_return.size() > 0) rename(text_return); return 0;}, XK_r, win::MOD_NONE },
    { "ext_rename", ctlm {std::string text_return = this->text_prompt("rename including extension", text_type::DEST); if (text_return.size() > 0) rename(text_return, true); return 0;}, XK_r, win::MOD_SHIFT },
    { "seek", ctlm {std::string text_return = this->text_prompt("enter position", text_type::POSITION); if (text_return.size() > 0) seek(std::stoi(text_return)); return 0;}, XK_s, win::MOD_NONE },
    { "create_new_dir", ctlm {std::string text_return = this->text_prompt("enter new directory", text_type::DEST); if (text_return.size() > 0) create_new_dir(text_return); return 0;}, XK_n, win::MOD_NONE },
    { "selection", ctlm {this->selection_toggle(); return 0;}, XK_slash, win::MOD_NONE},
    { "copy", ctlm {this->toggle_copy_mode(1); return 0;}, XK_c, win::MOD_CTRL},
    { "hold_copy", ctlm {this->toggle_copy_mode(2); return 0;}, XK_c, win::MOD_SHIFT | win::MOD_CTRL},
    { "add_dest", ctlm {this->add_dest(); return 0;}, XK_a, win::MOD_NONE},
    { "change_dest", ctlm {this->change_dest(); return 0;}, XK_c, win::MOD_NONE},
    { "remove_dest", ctlm {this->remove_dest(); return 0;}, XK_x, win::MOD_CTRL},
#ifdef VID
    { "play_video", ctlm {this->play_video(this->pos); return 0;}, XK_j, win::MOD_NONE},
#endif
    { "stop", ctlm {if (dispatcher != nullptr) this->dispatcher->stop(); return 0;}, {{XK_q, win::MOD_NONE},{XK_Escape, win::MOD_NONE}} }
  };

public:
  image_window(const std::string & name, dests::dest_group & dl, files::filelist & l, const util::rect size = win::def_win_rect);

  void on_left_button_down(XButtonEvent & xbutton)
  {
    image_drag_set(xbutton, true);
  }
  void on_left_button_up(XButtonEvent & xbutton)
  {
    image_drag_set(xbutton, false);
  }

  void on_scroll_up(XButtonEvent & xbutton)
  {
    win::mod_t mod = win::convert_mod(xbutton.state);
    if (mod == win::MOD_SHIFT)
      {
	zoom_in();
	set_exposed();
      }
    else
      this->cycle(-1);
  }
  void on_scroll_down(XButtonEvent & xbutton)
  {
    win::mod_t mod = win::convert_mod(xbutton.state);
    if (mod == win::MOD_SHIFT)
      {
	zoom_out();
	set_exposed();
      }
    else
      this->cycle(1);
  }

  //! gets the current position of the filelist.
  int get_pos() const { return pos; }
  //! Sets the position of the filelist.
  void set_pos(const unsigned int i) { pos = i; cycle(0); }
  
  auto & current_image() { return images[pos]; }

  void on_render();

  void on_client_message(XClientMessageEvent & xclient);

  void on_mouse_move(XMotionEvent & xmotion);

  void on_key_press(KeySym key, win::mod_t mod, XKeyEvent & xkey);
};

#endif //IMAGE_WINDOW_HPP
