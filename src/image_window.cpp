#include <unordered_map>
#include <memory>
#ifdef VID
#include <unistd.h>
#include <sys/wait.h>
#include <sys/time.h>
#include <openssl/md5.h>
#include <libffmpegthumbnailer/videothumbnailerc.h>
#endif
#include "image_window.hpp"
#include "buffer.h"

#define TEXT_WIN_DEF_WIDTH(disp_w) (((int)disp_w)/4 + ((int)disp_w)/8)
#define TEXT_WIN_DEF_HEIGHT(disp_h) (((int)disp_h)/16)
#define TEXT_WIN_DEF_SIZE(disp_size) {TEXT_WIN_DEF_WIDTH(disp_size.get_width()), TEXT_WIN_DEF_HEIGHT(disp_size.get_height())}

//! Map containing the keycodes for destination keys.
std::unordered_map<KeySym, int> dest_keys = {
  { XK_KP_1, 0 },
  { XK_KP_2, 1 },
  { XK_KP_3, 2 },
  { XK_KP_4, 3 },
  { XK_KP_5, 4 },
  { XK_KP_6, 5 },
  { XK_KP_7, 6 },
  { XK_KP_8, 7 },
  { XK_KP_9, 8 },
  { XK_KP_0, 9 },
  { XK_1, 10 },
  { XK_2, 11 },
  { XK_3, 12 },
  { XK_4, 13 },
  { XK_5, 14 },
  { XK_6, 15 },
  { XK_7, 16 },
  { XK_8, 17 },
  { XK_9, 18 },
  { XK_0, 19 },
  { XK_F1, 20 },
  { XK_F2, 21 },
  { XK_F3, 22 },
  { XK_F4, 23 },
  { XK_F5, 24 },
  { XK_F6, 25 },
  { XK_F7, 26 },
  { XK_F8, 27 },
  { XK_F9, 28 },
  { XK_F10, 29 },
  { XK_F11, 30 },
  { XK_F12, 31 }
};

#ifdef VID
namespace video
{
  bool check_extension(const std::string & filepath)
  {
    std::string ext = files::get_extname(filepath);

    for (unsigned int i = 0; i < extensions.size(); ++i)
      {
	if (ext == extensions.at(i))
	  {
	    return true;
	  }
      }

    return false;
  }

  /** @brief creates video preview.
   * @todo add safety features and failure notifications
   *
   * @param filepath path to file to generate preview for
   * @param previewpath path to place preview in
   */
  void create_preview(const std::string & filepath, const std::string & previewpath)
  {
    video_thumbnailer * thumbnail = video_thumbnailer_create();
    
    thumbnail->thumbnail_size = 512;
    thumbnail->seek_percentage = 3;
    thumbnail->overlay_film_strip = 1;
    
    video_thumbnailer_generate_thumbnail_to_file(thumbnail,
						 filepath.c_str(),
						 previewpath.c_str());
    
    video_thumbnailer_destroy(thumbnail);
    
    return;
  }

  std::string create_digest_string(const unsigned char * digest)
  {
    char * digest_string = (char*) malloc(sizeof(char) * 65);
    int i;
    
    for (i = 0; i < MD5_DIGEST_LENGTH; ++i)
      {
	sprintf((digest_string + (2*i)), "%02x", digest[i]);
      }
    *(digest_string + 64) = '\0';
    
    std::string std_digest_string = digest_string;
    free(digest_string);
    
    return std_digest_string;
  }

#define create_digest_string_from_string(path) create_digest_string(MD5((const unsigned char *)path.c_str(), path.size(), NULL))

  int delete_preview(const std::string & file_name)
  {
    int success = 0;
    
    if ((success = unlink(file_name.c_str())) != 0)
      std::cout << "unable to delete preview file\n";
    
    return(success);
  }
  
  std::string find_preview(const std::string & filepath)
  {
    struct stat vid_stat, preview_stat;
    std::string preview_name = create_digest_string_from_string(filepath);
    std::string preview_path = files::cat_path(getenv("HOME"), CACHE_DIR_LOCATION) + preview_name + ".png";

    if (files::check_file_exists(preview_path))
      {
	int diff_time = 0;
	stat(filepath.c_str(), &vid_stat);
	stat(preview_path.c_str(), &preview_stat);

	diff_time = vid_stat.st_mtime - preview_stat.st_mtime;
	
	if (diff_time <= 0)
	  return preview_path;
	else
	  delete_preview(preview_path);
      }

    create_preview(filepath, preview_path);
    return preview_path;
  }
}
#endif

class text_window : public imlib::imlib_window,
		    public win::text_input_win
{
private:
  std::list<std::string> & history;
  std::list<std::string>::iterator hist_i = history.begin();
  int hist_pos = -1;
  text_type type;
  files::dirlist completion_dirs;
  std::string comp_path;
  int completion_index = -1;
  bool slide_up = false;
  unsigned int default_height;

  unsigned int input_line_render(imlib::image & buf)
  {
    std::string render_string = ">" + text_buffer;
    util::rect text_size = buf.get_text_size(render_string);
    util::rect win_size(get_local_size());
    int offset = 0;
    if (text_size.get_width() > win_size.get_width())
      {
	offset = text_size.get_width() - win_size.get_width() + 4;
      }

    buf.draw_text({-offset+3,3}, render_string, {0,0,0});
    return (buf.draw_text_return({-offset + 2,2}, render_string, {255,255,255}).get_height());
  }

  void completionlist_render(imlib::image & buf, const unsigned int h)
  {
    util::rect win_size(get_local_size());
    std::vector<std::string> grid;
    int lines = 0;
    unsigned int total_height = h;
    static constexpr int height_buffer = 5;

    grid.emplace_back();
    grid.at(lines) = completion_dirs[0];
    for (unsigned int i = 1; i < completion_dirs.size(); ++i)
      {
	if (grid.at(lines).size() > 0)
	  {
	    util::rect line_dim  = buf.get_text_size(grid.at(lines) + " | " + completion_dirs[i]);
	    if (line_dim.get_width() > win_size.get_width()-4)
	      {
		grid.emplace_back();
		lines++;
		grid.at(lines) = completion_dirs[i];
		total_height += line_dim.get_height();
	      }
	    else
	      {
		grid.at(lines) += " | " + completion_dirs[i];
	      }
	  }
      }

    if (win_size.get_height() <= total_height)
      {
	if (slide_up == true)
	  {
	    util::rect p_size = parent.get_local_size();

	    if (total_height + height_buffer > p_size.get_height())
	      {
		total_height = p_size.get_height() - p_size.get_height()/10 - height_buffer;
	      }

	    move(get_pos().x, p_size.get_height() - total_height + height_buffer);
	  }
	resize(win_size.get_width(), total_height + height_buffer);
      }
    else if (win_size.get_height() > total_height + height_buffer)
      {
	if (slide_up == true)
	  {
	    move(get_pos().x, parent.get_local_size().get_height() - default_height);
	  }
	resize(win_size.get_width(), default_height);
      }

    buf.draw_text({3,(int)h+1}, grid, {0,0,0});
    buf.draw_text({2,(int)h}, grid, {255,255,255});
  }

  std::string compare_candidates(const std::string & prefix)
  {
    std::string new_prefix = prefix;

    for (unsigned int i = new_prefix.size(); i < completion_dirs[0].size(); ++i)
      {
	for (unsigned int j = 1; j < completion_dirs.size(); ++j)
	  {
	    if (completion_dirs[0][i] != completion_dirs[j][i])
	      {
		goto done;
	      }
	  }
	new_prefix += completion_dirs[0][i];
      }
  done:
    
    return new_prefix;
  }

  void completion_create()
  {
    comp_path = text_buffer.substr(0, text_buffer.find_last_of('/') + 1);
    std::string prefix  = text_buffer.substr(text_buffer.find_last_of('/')+1, text_buffer.npos);
    completion_dirs = files::dirlist(comp_path, prefix);

    if (prefix.size() != 0)
      {
	if (completion_dirs.size() == 0)
	  {
	    return;
	  }
	else if (completion_dirs.size() == 1)
	  {
	    text_buffer = comp_path + completion_dirs[0] + '/';
	    completion_dirs.clear();
	  }
	else
	  {
	    prefix = compare_candidates(prefix);
	    text_buffer = comp_path + prefix;
	  }
      }
  }

  void completion(const int i)
  {
    if (completion_dirs.size() != 0)
      {
	if (i == -1)
	  {
	    if (completion_index > 0)
	      {
		completion_index--;
	      }
	    else
	      {
		completion_index = (int)completion_dirs.size() - 1;
	      }
	  }
	else
	  {
	    completion_index++;
	    if (completion_index == (int)completion_dirs.size())
	      {
		completion_index = 0;
	      }
	  }

	text_buffer = comp_path + completion_dirs[completion_index];
      }
    else
      {
	completion_create();
	completion_index = -1;
      }
  }
  
public:
  text_window(const std::string & name, std::list<std::string> & hist, const text_type t)
    : win::window(name, {460,100}), history(hist), type(t)
  {
    default_height = get_local_size().get_height();
    atoms.push_back({ XInternAtom(display->get_disp(), "WM_DELETE_WINDOW", 0) });
    XSetWMProtocols(display->get_disp(), screen, atoms.data(), 1);

    //set icon
    Atom net_wm_icon = XInternAtom(display->get_disp(), "_NET_WM_ICON", False);
    Atom cardinal = XInternAtom(display->get_disp(), "CARDINAL", False);
    XChangeProperty(display->get_disp(), id(), net_wm_icon, cardinal, 32, PropModeReplace, (const unsigned char *) buffer, length);
  }
  /** 
   * @todo Fix text input only working when pointer is over window.
   */
  text_window(win::window & p, std::list<std::string> & hist, const text_type t)
    : win::window(p, {0,(int)(p.get_local_size().get_y()+(2*p.get_local_size().get_height())/3),(int)p.get_local_size().get_width(),(int)p.get_local_size().get_height()/3}), history(hist), type(t), slide_up(true)
  {
    default_height = get_local_size().get_height();
  }

  void on_render()
  {
    if (slide_up == true)
      {
	parent.on_render();
      }
    
    unsigned int h = 0;
    imlib::image buffer(get_local_size());
    buffer.fill_image({5,5,5});

    h = input_line_render(buffer);
    if (completion_dirs.size() > 0)
      {
	completionlist_render(buffer, h);
      }

    buffer.render(screen);

  }

  void on_client_message(XClientMessageEvent &xclient)
  {
    if ((Atom)xclient.data.l[0] == atoms[0])
      {
	text_buffer.clear();
	if (dispatcher != nullptr)
	  dispatcher->stop();
      }
  }
  
  void on_return()
  {
    if (history.front() != text_buffer)
      {
	history.push_front(text_buffer);
      }
    if (dispatcher != nullptr)
      dispatcher->stop();
  }

  void on_key_press(KeySym key, win::mod_t mod, XKeyEvent & xkey)
  {
    if (key == XK_c && mod == win::MOD_CTRL)
      {
	text_buffer.clear();
	if (dispatcher != nullptr)
	  dispatcher->stop();
	return;
      }
    if (type == text_type::DEST && (key == XK_Tab || key == XK_ISO_Left_Tab))
      {
	if (mod == win::MOD_SHIFT)
	  {
	    completion(-1);
	  }
	else if (mod == win::MOD_NONE)
	  {
	    completion(1);
	  }
	set_exposed();
	return;
      }
    if (key == XK_Up && mod == win::MOD_NONE)
      {
	if (hist_pos < (int)history.size()-1)
	  {
	    if (hist_pos != -1)
	      {
		++hist_i;
	      }
	    text_buffer = *hist_i;
	    ++hist_pos;
	  }
	set_exposed();
	return;
      }
    if (key == XK_Down && mod == win::MOD_NONE)
      {
	if (hist_pos > 0)
	  {
	    --hist_i;
	    text_buffer = *hist_i;
	    --hist_pos;
	  }
	set_exposed();
	return;
      }

    std::string::size_type buf_size = text_buffer.size();
    text_input_win::on_key_press(key, mod, xkey);
    if (text_buffer.size() != buf_size)
      {
	completion_dirs.clear();
	completion_index = -1;
      }
  }

  std::string get_text_string() const { return text_buffer; }
};

bool image_window::index(const int i)
{
  bool found = false;
  if ((pos + i) < 0) { return false; }
  int new_pos = pos + i;
  int index_direction = 1;
  if (i < 0)
    {
      index_direction = -1;
    }
  
  while ((!found) && (new_pos >= 0) && ((unsigned int)(new_pos) < images.size()))
    {
      if (images[new_pos].removed == true)
	{
	  new_pos += index_direction;
	}
      else if (!(imlib::image::test_image(images.get_path() + images[new_pos].name)))
	{
#ifdef VID
	  if (video::check_extension(images.get_path() + images[new_pos].name))
	    {
	      found = true;
	      pos = new_pos;
	    }
	  else
	    {
#endif
	      images[new_pos].removed = true;
	      new_pos += index_direction;
#ifdef VID
	    }
#endif
	}
      else
	{
	  found = true;
	  pos = new_pos;
	}
    }
  
  return found;
}

void image_window::generate_title()
{
  std::string title = "";

  if (copy_mode == 1)
    {
      title += "c - ";
    }
  else if (copy_mode == 2)
    {
      title += "C - ";
    }
  
  if (selection_state.enabled == true)
    {
      int scroll_len;
      if (pos >= selection_state.start)
	{
	  scroll_len = pos - selection_state.start + 1;
	}
      else
	{
	  scroll_len = selection_state.start - pos;
	}

      title += "scroll: " + std::to_string(scroll_len) + ' ';
    }
  
  title += '[' + std::to_string(pos+1) + " - " + std::to_string(images.size());
  if (images.removed() != 0)
    {
      title += "(-" + std::to_string(images.removed()) + ')';
    }
  title += "] - " + images[pos].name;

  set_title(title);
}

void image_window::cycle(const int i)
{
  if (index(i))
    {
      reset_zoom();
      drag_point = {0,0};
      generate_title();
      set_exposed();
    }
  else
    {
      if (dispatcher != nullptr)
	dispatcher->stop();
    }
}

void image_window::cycle_if_removed(const int i)
{
  if (images[pos].removed == true)
    {
      cycle(i);
    }
}

void image_window::seek(const unsigned int i)
{
  if (i < 1 || i > images.size())
    {
      std::cout << "out of range\n";
      return;
    }
  pos = i-1;

  cycle(0);
}

void image_window::cycle_destlist(const int i)
{
  if (i == 1)
    {
      if (current_dest_list < dest_lists.size()-1)
	{
	  current_dest_list++;
	}
    }
  else
    {
      if (current_dest_list > 0)
	{
	  current_dest_list--;
	}
    }

  set_exposed();
}

std::vector<std::string> image_window::format_dests(dests::destlist & d)
{
  std::vector<std::string> formatted_dests;
  
  auto key_name_translate = [](const int n)
      {
	if (n <= 9) return (std::string("KP_") + std::to_string(n));
	else if (n == 10) return (std::string("KP_0"));
	else if (n <= 19) return (std::string("") + std::to_string(n-10));
	else if (n == 20) return (std::string("0"));
	else return (std::string("f") + std::to_string(n-20));
      };
  
  for (decltype(d.get_display_dests().size()) i = 0; i < d.get_display_dests().size(); ++i)
    {
      formatted_dests.push_back(std::to_string(i+1)+" - "+key_name_translate(i+1)+" - "+d.get_display_dests().at(i));
    }
  
  return formatted_dests;
}

std::string image_window::text_prompt(const std::string & title, text_type t)
{
  std::string return_text;
  std::unique_ptr<text_window> t_win = nullptr;
  std::list<std::string> * hist = nullptr;
  if (t == text_type::DEST)
    {
      hist = &dest_hist;
    }
  else if (t == text_type::POSITION)
    {
      hist = &pos_hist;
    }
  else
    {
      hist = &null_hist;
    }

  win::event_dispatcher ev(get_display());
  if (g_flags.slide_up == true)
    {
      t_win = std::make_unique<text_window>(*this, *hist, t);
    }
  else
    {
      t_win = std::make_unique<text_window>(title, *hist, t);
    }
  win::event_handle handle(t_win.get(), &ev);
  t_win->on_render();
  t_win->show();
  
  ev.run();
  
  t_win->hide();

  if (t == text_type::NONE)
    {
      null_hist.clear();
    }

  return_text = t_win->get_text_string();
  
  return return_text;
}

void image_window::selection_toggle()
{
  if (selection_state.enabled == false)
    {
      selection_state.start = pos;
      selection_state.enabled = true;
      generate_title();
    }
  else
    {
      selection_state.enabled = false;
      generate_title();
    }
}

void image_window::move(const std::string & dest)
{
  if (dest.size() == 0)
    {
      return;
    }
  
  if (selection_state.enabled == true)
    {
      if (selection_state.start <= pos)
	{
	  for (unsigned int i = selection_state.start; i <= pos; ++i)
	    {

	      if (images[i].removed == false && !(imlib::image::test_image(images.get_path() + images[i].name))
#ifdef VID
		  && !(video::check_extension(images[i].name))
#endif
		  )
		{
		  images[i].removed = true;
		}
	    }
	  images.multi_move(selection_state.start, pos, dest);
	}
      else
	{
	  for (unsigned int i = pos; i <= selection_state.start; ++i)
	    {
	      if (images[i].removed == false && !(imlib::image::test_image(images.get_path() + images[i].name))
#ifdef VID
		  && !(video::check_extension(images[i].name))
#endif
		  )
		{
		  images[i].removed = true;
		}
	    }
	  images.multi_move(pos, selection_state.start, dest);
	}
      selection_toggle();
    }
  else
    {
      if (copy_mode != 0)
	{
	  if (images[pos].removed == false && !(imlib::image::test_image(images.get_path() + images[pos].name))
#ifdef VID
	      && !(video::check_extension(images[pos].name))
#endif
	      )
	    {
	      images[pos].removed = true;
	    }
	  images.copy(pos, dest);
	  if (copy_mode == 1)
	    {
	      toggle_copy_mode(0);
	    }
	}
      else
	{
	  if (images[pos].removed == false && !(imlib::image::test_image(images.get_path() + images[pos].name))
#ifdef VID
	      && !(video::check_extension(images[pos].name))
#endif
	      )
	    {
	      images[pos].removed = true;
	    }
	  images.move(pos, dest);
	}
    }

  cycle_if_removed(1);
}

void image_window::rename(const std::string & newname, bool include_ext)
{
  if (newname.size() == 0)
    {
      return;
    }

  images.rename(pos, newname, include_ext);
  generate_title();
}

void image_window::remove()
{
  images.remove(pos);
  cycle_if_removed(1);
}

void image_window::add_dest()
{
  std::string text_return = text_prompt("enter new destination", text_type::DEST);

  if (text_return.size() > 0)
    {
      if (dests::real_path(text_return) == "")
	{
	  return;
	}
      else if (dest_lists.size() == 0)
	{
	  std::vector<std::string> d_add;
	  d_add.push_back(text_return);
	  dest_lists.add(d_add);
	  current_dest_list = 0;
	}
      else
	{
	  dest_lists[current_dest_list].append(text_return);
	}
      set_exposed();
    }
}

void image_window::change_dest()
{
  if (current_dest_list == -1)
    {
      return;
    }
  dests::destlist & d_list = dest_lists[current_dest_list];

  std::string list_pos_return = text_prompt("enter destlist position", text_type::POSITION);

  if (list_pos_return == "")
    {
      return;
    }
  
  int list_pos = std::stoi(list_pos_return)-1;

  if (list_pos < 0 || list_pos >= d_list.size())
    {
      return;
    }
  
  std::string text_return = text_prompt("enter new destination", text_type::DEST);

  if (text_return.size() > 0)
    {
      if (dests::real_path(text_return) == "")
	{
	  return;
	}
      else
	{
	  d_list.change(list_pos, text_return);
	}
      set_exposed();
    }
}

void image_window::remove_dest()
{
  if (current_dest_list == -1)
    {
      return;
    }
  dests::destlist & d_list = dest_lists[current_dest_list];

  d_list.remove();
  set_exposed();
}

#ifdef VID
void image_window::play_video(const int i)
{
  const std::string path = files::cat_path(images.get_path(), images[i].name);

  if (!(video::check_extension(path)))
    {
      std::cout << "not a video file\n";
      return;
    }
  std::string command_string = g_settings.video_player;
  command_string += " \'" + path + "\'";

  if (system(command_string.c_str()) == -1)
    {
      std::cout << "video player failed to launch\n";
      return;
    }

  return;
}
#endif

void image_window::create_new_dir(const std::string & dir)
{
  if(mkdir(dir.c_str(), S_IRWXU | S_IRWXG | S_IROTH | S_IXOTH) == -1)
    {
      std::cout << "couldn't create directory\n";
    }
}

image_window::image_window(const std::string & name, dests::dest_group & dl, files::filelist & l, const util::rect size)
    : win::window(name, size), dest_lists(dl), images(l)
  {
    atoms.push_back({ XInternAtom(display->get_disp(), "WM_DELETE_WINDOW", 0) });
    XSetWMProtocols(display->get_disp(), screen, atoms.data(), 1);

    //set icon
    Atom net_wm_icon = XInternAtom(display->get_disp(), "_NET_WM_ICON", False);
    Atom cardinal = XInternAtom(display->get_disp(), "CARDINAL", False);
    XChangeProperty(display->get_disp(), id(), net_wm_icon, cardinal, 32, PropModeReplace, (const unsigned char *) buffer, length);
    
    c_list.generate_bindings(def_controls);
    if (dest_lists.size() > 0)
      {
	current_dest_list = 0;
      }

    cycle(0);
  }

void image_window::on_render()
{
  util::rect win_size(get_local_size());
  imlib::image buffer(win_size);
  buffer.resize(win_size);
  buffer.fill_image({50,50,50});
  
  util::rect size = win_size*zoom_factor;
  size.set_center(win_size.get_center() + drag_point);
#ifdef VID
  if (video::check_extension((images.get_path() + current_image().name)))
    {
      buffer.load_image_scaled(video::find_preview((images.get_path() + current_image().name)),
			       size);
    }
  else
    {
#endif
      buffer.load_image_scaled((images.get_path() + current_image().name),
			   size);
#ifdef VID
    }
#endif
  if (current_dest_list >= 0 && show_dest_list == true)
    {
      std::vector<std::string> formatted_dests = format_dests(dest_lists[current_dest_list]);
      buffer.draw_text({3,3}, formatted_dests, {0,0,0});
      buffer.draw_text({2,2}, formatted_dests, {255,255,255});
    }
  buffer.render(screen);
}

void image_window::on_client_message(XClientMessageEvent &xclient)
{
  if ((Atom)xclient.data.l[0] == atoms[0])
    {
      if (dispatcher != nullptr)
	dispatcher->stop();
    }
}

void image_window::on_mouse_move(XMotionEvent & xmotion)
{
  if (drag == true)
    {
      util::point new_pointer(xmotion.x_root, xmotion.y_root);
      drag_point += (new_pointer - pointer);
      pointer = new_pointer;
      set_exposed();
    }
}

void image_window::on_key_press(KeySym key, win::mod_t mod, XKeyEvent & xkey)
{
  if (mod == win::MOD_NONE)
    if (dest_keys.find(key) != dest_keys.end())
      {
	if (current_dest_list != -1)
	  {
	    const std::vector<std::string> & list = dest_lists[current_dest_list].get_dests();
	    std::vector<std::string>::size_type i = dest_keys.at(key);
	    if (i < list.size())
	      {
		move(list.at(i));
	      }
	  }
	return;
      }
  
  win::multifunction_win::on_key_press(key, mod, xkey);
}
