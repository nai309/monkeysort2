#include "destlist.hpp"
#include <fstream>
#include <cstring>
#include <stdlib.h>
#include <limits.h>
#include <unistd.h>


namespace dests
{
  std::string real_path(const std::string & rel_path)
  {
    char * full_path_p = realpath(rel_path.c_str(), NULL);
    if (!full_path_p)
      {
	return "";
      }
    std::string full_path(full_path_p);

    free(full_path_p);

    return full_path;
  }

  void destlist::load(const std::string & destfile)
  {
    std::ifstream d_file(destfile);
    if (d_file.is_open() == false)
      {
	throw excep::failed_to_open_destfile();
      }
    std::vector<std::string> dests;
    std::string line;
    
    while(d_file.eof() != true)
      {
	std::getline(d_file, line, '\n');
	if (line.size() > 0)
	  dests.push_back(line);
      }
    d_file.close();
    
    char * current_wd = getcwd(NULL, 0);
    char * list_wd = realpath(destfile.c_str(), NULL);
    char * last_chr = strrchr(list_wd, '/');
    *(last_chr + 1) = '\0';
    chdir(list_wd);

    fill(dests);
    
    chdir(current_wd);
    
    free(current_wd);
    free(list_wd);
  }

}
