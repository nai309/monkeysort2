#ifndef FILELIST_HPP
#define FILELIST_HPP
#include <vector>
#include <list>
#include <string>
#include <iostream>
#include <fstream>
#include <dirent.h>
#include <sys/stat.h>

#define UNDO_MAX 300

namespace excep
{
  //! Exception defining a class of errors related to file operations.
  class file_error : public std::exception {};
  //! Exception thrown when an invalid path is encountered.
  class bad_path : public std::exception {};
  //! Exception thrown when a directory failed to open.
  class failed_open_directory : public std::exception {};
  //! Exception thrown when a stat call on a file fails.
  class stat_failed : public file_error {};
  //! Exception thrown when a open call fails.
  class failed_open_file : public file_error {};
  //! Exception thrown when a sendfile call fails.
  class failed_copy_file : public file_error {};
  //! Exception thrown when a move operation fails.
  class failed_move_file : public file_error {};
  //! Exception thrown when a file already exists at destination location.
  class file_exists : public file_error {};
  //! Exception thrown when a file cannot be deleted.
  class failed_to_delete : public file_error {};
}


/**
 * Namespace for classes and functions related to file lists.
 */
namespace files
{
  //! Enum listing the supported sorting modes.
  enum sorts {
    TIME_SORT,
    REV_TIME_SORT,
    ALPHA_SORT,
    VERSION_SORT
  };

  //! Struct containing the name and mod time of a file.
  struct file_entry
  {
    time_t mtime;
    std::string name;
    bool removed = false;

    file_entry(const time_t & m, const std::string & n) : mtime(m), name(n) {}
    file_entry(const time_t & m, const char * n) : mtime(m), name(n) {}
  };

  bool check_file_exists(const std::string & path);
  std::string get_extname(const std::string & filename);

  /** @brief Constructs a full file path from the string \c path and \c d_name, inserting a path seperator if necessary.
   *
   * @param dir string containing the directory, with or withour trailing path seperator
   * @param filename string containing the filename
   * @return string containing the full concatinated path
   */
  std::string cat_path(const std::string & dir, const std::string & filename);
  
  inline namespace undo
  {
    enum op_type {
      NONE,
      MOVE,
      COPY,
      RENAME,
      MULTI_START,
      MULTI_STOP
    };
    
    struct op_data
    {
      op_type type = op_type::NONE;
      file_entry * entry = nullptr;
      std::string srcpath;
      std::string destpath;

      op_data() {}
      op_data(const op_type type, file_entry * entry, const std::string & srcpath, const std::string & destpath)
	: type(type), entry(entry), srcpath(srcpath), destpath(destpath) {}
      op_data(const op_type type, file_entry * entry = nullptr)
	: type(type), entry(entry) {}
    };
    
    class undo_stack
    {
    private:
      static constexpr int max_size = UNDO_MAX;
      op_data default_node;
      std::list<op_data> stack;

      void trim()
      {
	if (stack.size() > max_size)
	  {
	    for (unsigned long i = 0; i < (stack.size() - max_size); ++i)
	      {
		stack.pop_back();
	      }
	  }
      }

    public:
      undo_stack(){}
      
      void push(const op_type type, file_entry * entry,
		const std::string & srcpath, const std::string & destpath);
      
      void push(const op_type type);

      op_data & get_front()
      {
	if (stack.size() > 0)
	  {
	    return stack.front();
	  }
	else
	  {
	    return default_node;
	  }
      }

      void pop() { stack.pop_front(); }
    };
    
  }

  enum overwrite_behavior {
    NONE,
    OVER,
    RENAME
  };
  
  /** @brief Class defining a list of files in a given path.
   * 
   */
  class filelist
  {
  private:
    std::string path;
    int rem = 0;
    std::vector<file_entry> files;
    undo_stack undos;
    overwrite_behavior behavior = overwrite_behavior::NONE;

    void copy_file(const std::string & srcpath, const std::string & destpath);
    void move_file(const std::string & srcpath, const std::string & destpath);
    void remove_file(const std::string & srcpath);
    void undo_move(op_data & data);
    void undo_copy(const op_data & data);
    void undo_rename(op_data & data);
  public:
    //! Default constructor for \c filelist class.
    filelist() {}
    /** @brief Constructor for \c filelist class.
     *
     * @param src_path pathname to directory collect files from
     * @param sorting value from enum defining sorting style for files
     * @param filter string defining an optional filter so that only matching files are adde to the filelist
     */
    filelist(const std::string & src_path, const sorts sorting, const std::string & filter = "");
    //! Gets the source path of the filelist.
    const std::string & get_path() const { return path; }
    //! Getter for the length of the filelist.
    auto size() const { return files.size(); }
    //! Getter for the number of files removed from the filelist.
    auto removed() const { return rem; }
    //! Indexes into the filelist.
    auto & operator [] (const decltype(files)::size_type i)
    { return files.at(i); }
    //! Sets the overwrite behavior for the \c filelist.
    void set_behavior(const overwrite_behavior b) { behavior = b; }
    /** @brief Moves the file in position \c i to the given destination.
     *
     * @param i position in the \c filelist of the file to move
     * @param dest destination directory to move the file to
     */
    void move(const decltype(files)::size_type i, const std::string & dest);
    /** @brief Copies the file in position \c i to the given destination.
     *
     * @param i position in the \c filelist of the file to copy
     * @param dest destination directory to copy the file to
     */
    void copy(const decltype(files)::size_type i, const std::string & dest);
    /** @brief Renames the file in position \c i to the given filename.
     *
     * @param i position in the \c filelist of the file to rename
     * @param newname new filename to rename the file to
     * @param include_ext whether to append the existing file extention onto \c newname
     */
    void rename(const decltype(files)::size_type i, const std::string & newname, const bool include_ext = false);
    /** @brief Deletes the file in position \c i.
     *
     * @param i position in the \c filelist of the file to delete
     */
    void remove(const decltype(files)::size_type i);
    /** @brief Moves a range of files to the given destination.
     *
     * @param begin position of the first file to move
     * @param end position of the last file to move
     * @param dest destination directory to move the files to
     */
    void multi_move(const decltype(files)::size_type begin, const decltype(files)::size_type end, const std::string & dest);
    //! Undoes the last operation performed on the filelist.
    void undo();
  };

  /** @brief Class defining a list of directories found in a given path.
   *
   */
  class dirlist
  {
  private:
    //! Path where directories were searched for.
    std::string path;
    //! Vector of directories found.
    std::vector<std::string> dirs;
  public:
    //! Default constructor for \c dirlist class.
    dirlist() {}
    /** @brief Constructor for \c dirlist class.
     *
     * @param p path to look for directories in
     * @param prefix string prefix to filter found directories against
     */
    explicit dirlist(const std::string & p, const std::string & prefix = "");

    //! Index operator to access directories in the \c dirlist.
    std::string operator [](const std::string::size_type i) { return dirs.at(i); }
    //! Returns the size of the \c dirlist.
    std::vector<std::string>::size_type size() { return dirs.size(); }
    //! Empties \c dirlist.
    void clear() { dirs.clear(); path.clear(); }
    
  };
}


#endif //FILELIST_HPP
