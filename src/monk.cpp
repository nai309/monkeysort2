#include <cstdio>
#include <iostream>
#include <getopt.h>
#include <unistd.h>
#include <windowing/settings.hpp>
#include "general_settings.hpp"
#include "image_window.hpp"


//environment setup
win::x_display main_display;
win::event_dispatcher events(&main_display);
setting::settings_register reg;

imlib::imlib_env environment(main_display);
//end environment setup

enum long_vals {
  NO_DEF_DEST,
  GEN_CONF
};

static struct option long_opt_list[] = {
  {"alphabetic", no_argument, NULL, 'a'},
  {"recent", no_argument, NULL, 'u'},
  {"overwrite", no_argument, NULL, 'o'},
  {"rename", no_argument, NULL, 'r'},
  {"slide-up-text", no_argument, NULL, 't'},
  {"no-default-destfile", no_argument, NULL, NO_DEF_DEST},
  {"destfile", required_argument, NULL, 'd'},
  {"working-directory", required_argument, NULL, 'w'},
  {"config", required_argument, NULL, 'c'},
  {"start", required_argument, NULL, 's'},
  {"generate-config", required_argument, NULL, GEN_CONF},
  {"filter", required_argument, NULL, 'f'}
};

void change_wd(const std::string & wd);
void check_for_user_dirs();
void config_generation(const std::string & path);

int main(int argc, char **argv)
{
  files::sorts sort_mode = files::TIME_SORT;
  files::overwrite_behavior behavior = files::overwrite_behavior::NONE;
  std::string filter = "";
  int option = 0, long_ind = 0;
  dests::dest_group image_group;
  std::string conf_file = "";
  bool use_def_dest_file = true;
  std::vector<std::string> destfiles;
  int start_pos = 1;
  
  if (argc < 2)
    {
      std::cout << "usage: " << PRGM_NAME << " [OPTIONS] SRC_DIR [DEST_DIR(1-32)]\n";
      return 1;
    }

  while ((option = getopt_long(argc,argv, "auortd:w:c:s:f:", long_opt_list, &long_ind)) != -1)
    {
      switch (option)
	{
	case 'a': //sort alphabetically (uses versionsort)
	  sort_mode = files::VERSION_SORT;
	  break;
	case 'u': //sort from newest to oldest
	  sort_mode = files::REV_TIME_SORT;
	  break;
	case 'o':
	  behavior = files::overwrite_behavior::OVER;
	  break;
	case 'r':
	  behavior = files::overwrite_behavior::RENAME;
	  break;
	case 't':
	  g_flags.slide_up = true;
	  break;
	case NO_DEF_DEST:
	  use_def_dest_file = false;
	  break;
	case 'd':
	  destfiles.push_back(optarg);
	  break;
	case 'w':
	  change_wd(optarg);
	  break;
	case 'c':
	  conf_file = optarg;
	  break;
	case 's':
	  start_pos = std::stoi(optarg);
	  break;
	case GEN_CONF:
	  config_generation(optarg);
	  exit(0);
	  break;
	case 'f':
	  filter = optarg;
	default:
	  break;
	}
    }

  if (argc <= optind)
    {
      std::cout << "no source folder specified\n";
      exit(1);
    }

  check_for_user_dirs();
  
  files::filelist images(argv[optind], sort_mode, filter);
  images.set_behavior(behavior);

  if (images.size() == 0)
    {
      std::cout << "Directory was either empty, or there were no results based on the given filter, exiting.\n";
      exit(1);
    }

  {
    if (use_def_dest_file == true)
      {
	std::string def_dest_file = argv[optind] + std::string(DEF_DEST_FILE_NAME);
	if (files::check_file_exists(def_dest_file) == true)
	  {
	    image_group.add(def_dest_file);
	  }
      }
    
    for (auto x : destfiles)
      {
	image_group.add(x);
      }
    
    std::vector<std::string> dests;
    for (int i = optind+1; i < argc; ++i)
      {
	dests.push_back(argv[i]);
      }

    if (dests.size() > 0)
      {
	if (image_group.size() > 0)
	  {
	    image_group[0].append(dests);
	  }
	else
	  {
	    image_group.add(dests);
	  }
      }
  }

  set_def_display(&main_display);
  image_window im_win("images", image_group, images);
  win::event_handle im_win_handle(&im_win, &events);
  setting::control_category main_controls("controls", im_win.def_controls);
  reg.add_category(main_controls);
  general_category g_category;
  reg.add_category(g_category);

  if (conf_file == "")
    {
      conf_file = files::cat_path(getenv("HOME"), USER_CONF_LOCATION);
      if (!files::check_file_exists(conf_file))
	{
	  conf_file = DEF_CONF_LOCATION;
	}
    }
  if (files::check_file_exists(conf_file))
    {
      reg.process_settings_file(conf_file);
    }
  im_win.controls().set_bindings(main_controls.return_binds());
  im_win.resize(g_settings.win_w,g_settings.win_h);

  if (start_pos < 1 || start_pos > (int)images.size())
    {
      std::cout << "position out of range\n";
      exit(1);
    }
  else
    {
      im_win.set_pos(start_pos-1);
    }
  
  im_win.on_render();
  im_win.show();

  events.run();

  //program end output
  std::cout << "file list: " << images.get_path() << '\n';
  std::cout << (im_win.get_pos()+1) << " - " << images.size() << '\n';
  std::cout << (im_win.get_pos() - images.removed()+1) << " - " << (images.size() - images.removed()) << '\n';
}




void change_wd(const std::string & wd)
{
  char * real_wd = realpath(wd.c_str(), NULL);

  if (real_wd != NULL)
    {
      if (!chdir(real_wd))
	{
	  free(real_wd);
	  return;
	}
    }

  free(real_wd);
  std::cout << "failed to set working directory, exiting\n";
  exit(1);
}

void check_for_user_dirs()
{
  std::string config_dir = files::cat_path(getenv("HOME"), USER_DIR_LOCATION);
#ifdef VID
  std::string cache_dir = files::cat_path(getenv("HOME"), CACHE_DIR_LOCATION);
#endif

  if (!(files::check_file_exists(config_dir)))
    {
      if (mkdir(config_dir.c_str(), S_IRWXU | S_IRWXG | S_IROTH | S_IXOTH) != 0)
	{
	  std::cout << "could not create user config directory\n";
	}
    }

#ifdef VID
  if (!(files::check_file_exists(cache_dir)))
    {
      if (mkdir(cache_dir.c_str(), S_IRWXU | S_IRWXG | S_IROTH | S_IXOTH) != 0)
	{
	  std::cout << "could not create preview cache directory\n";
	}
    }
#endif
}

void config_generation(const std::string & path)
{
  if (path != "")
    {
      if (!(files::check_file_exists(path)))
	{
	  set_def_display(&main_display);
	  setting::settings_register gen_reg;
	  files::filelist gen_list;
	  dests::dest_group gen_dests;
	  image_window gen_window("gen", gen_dests, gen_list);
	  setting::control_category main_controls("controls", gen_window.def_controls);
	  gen_reg.add_category(main_controls);
	  general_category g_category;
	  gen_reg.add_category(g_category);

	  gen_reg.generate_settings_file(path);
	}
      else
	{
	  std::cout << "file already exists.\n";
	}
    }
}
