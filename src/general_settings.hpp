#ifndef GENERAL_SETTINGS_HPP
#define GENERAL_SETTINGS_HPP
#include <unordered_map>
#include <functional>
#include <windowing/settings.hpp>

#define DEFAULT_WIN_WIDTH 1024
#define DEFAULT_WIN_HEIGHT 768

#ifdef VID
#define DEFAULT_VIDEO_PLAYER "mpv -fs"
#endif

struct general_settings
{
  int win_w = DEFAULT_WIN_WIDTH;
  int win_h = DEFAULT_WIN_HEIGHT;
  #ifdef VID
  std::string video_player = DEFAULT_VIDEO_PLAYER;
  #endif
};

struct general_flags
{
  bool slide_up = false;
};

extern general_settings g_settings;

extern general_flags g_flags;

class general_category : public setting::category
{
private:
  static void set_win_size(const std::string & s);

#ifdef VID
  static void set_video_player(const std::string & s)
  {
    g_settings.video_player = s;
  }
#endif

  std::unordered_map<std::string,std::function<void(const std::string &)>>
  gen_setting_map = {
    #ifdef VID
    { "video_player", set_video_player },
    #endif
    { "window_size", set_win_size }
  };
public:
  general_category() : setting::category("general") {}
  void process_item(const std::string & s)
  {
    std::string::size_type n = s.find(" ");
    const std::string & key = s.substr(0,n);
    const std::string & val = s.substr(n+1);
    auto end = gen_setting_map.end();
    auto item = gen_setting_map.find(key);
    if (item != end)
      {
	item->second(val);
      }
  }
  void generate_config(std::ofstream & of);
};






#endif //GENERAL_SETTINGS_HPP
