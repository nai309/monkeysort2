#include "general_settings.hpp"

general_settings g_settings;

general_flags g_flags;

void general_category::set_win_size(const std::string & s)
{
  std::string::size_type end = s.npos;
  std::string::size_type comma = s.find(',');

  if (comma == end)
    {
      std::cout << "win_size value invalid\n";
      return;
    }

  g_settings.win_w = std::stoi(s.substr(0, comma));
  g_settings.win_h = std::stoi(s.substr(comma+1));
}

void general_category::generate_config(std::ofstream & of)
{
#ifdef VID
  of << "#video_player " << g_settings.video_player << '\n';
#endif
  of << "#window_size " << g_settings.win_w << ',' << g_settings.win_h << '\n';
}
