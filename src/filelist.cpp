#include "filelist.hpp"
#include <fnmatch.h>
#include <cstring>
#include <algorithm>
#include <cstdio>
#include <sys/types.h>
#include <sys/sendfile.h>
#include <fcntl.h>
#include <unistd.h>


namespace files
{
  bool check_file_exists(const std::string & path)
  {
    if (access(path.c_str(), F_OK) != -1)
      {
	return true;
      }
    else
      {
	return false;
      }
  }

  std::string get_extname(const std::string & filename)
  {
    std::string::size_type pos = filename.find_last_of('.');

    if (pos == filename.npos)
      {
	return "";
      }
    else
      {
	return filename.substr(pos);
      }
  }

  std::string cat_path(const std::string & dir, const std::string & filename)
  {
    std::string full_path;
    if (dir.back() == '/')
      {
	full_path = dir + filename;
      }
    else
      {
	full_path = dir + '/' + filename;
      }

    return full_path;
  }
  
  inline namespace undo
  {
    void undo_stack::push(const op_type type, file_entry * entry,
		const std::string & srcpath, const std::string & destpath)
      {
	stack.emplace_front(type, entry, srcpath, destpath);

	trim();
      }

    void undo_stack::push(const op_type type)
      {
	stack.emplace_front(type);

	trim();
      }

  }
  
  class filter_check
  {
  private:
    const std::string & filter;
    bool exist;
  public:
    filter_check(const std::string & fil) : filter(fil)
    {
      exist = (filter != "");
    }
    bool exists() { return exist; }
    int operator () (const struct dirent * dir)
    {
      int success;
      
      if (exist)
	success = fnmatch(filter.c_str(), dir->d_name, 0);
      else
	success = 0;
      
      if (success == 0)
	return 1;
      else
	return 0;
    }
  };

  class sort_function
  {
  private:
    sorts sort;
  public:
    sort_function(const sorts s) : sort(s) {}
    int operator() (const file_entry & entry_1, const file_entry & entry_2)
    {
      switch (sort)
	{
	case TIME_SORT:
	  return (entry_1.mtime < entry_2.mtime);
	  break;
	case REV_TIME_SORT:
	  return (entry_2.mtime < entry_1.mtime);
	  break;
	case ALPHA_SORT:
	  return (std::strcmp(entry_1.name.c_str(), entry_2.name.c_str())) < 0;
	  break;
	case VERSION_SORT:
	  return (strverscmp(entry_1.name.c_str(), entry_2.name.c_str())) < 0;
	  break;
	default:
	  return (0);
	  break;
	}
    }
  };

  /** @brief Wrapper function for calls to stat.
   *
   * @param path string for the directory path
   * @param d_name character pointer string for the filename
   * @param buf return buffer for the stat call
   * @return returns return code from the stat call
   */
  int get_stat(const std::string & path, const char * d_name, struct stat & buf)
  {   
    return (stat(cat_path(path, d_name).c_str(), &buf));
  }
  
  filelist::filelist(const std::string & src_path, const sorts sorting, const std::string & filter)
  {
    char * hold_path = realpath(src_path.c_str(), NULL);
    if (!hold_path)
      {
	throw excep::bad_path();
      }
    path = hold_path;
    if (path.back() != '/')
      {
	path += '/';
      }
    DIR * directory = opendir(hold_path);
    free(hold_path);
    if (!directory)
      {
	throw excep::failed_open_directory();
      }
    struct dirent * d = readdir(directory);
    filter_check filter_func(filter);
    struct stat buf;
    while (d)
      {
	if (d->d_type != DT_DIR)
	  {
	    if (filter_func(d))
	      {
		if(get_stat(path, d->d_name, buf))
		  {
		    throw excep::stat_failed();
		  }
		files.push_back(file_entry(buf.st_mtim.tv_sec, d->d_name));
	      }
	  }
	d = readdir(directory);
      }
    closedir(directory);

    sort_function sor(sorting);
    std::sort(files.begin(), files.end(), sor);
  }

  void filelist::copy_file(const std::string & srcpath, const std::string & destpath)
  {
    int src_fd, dest_fd, copy_size;
    struct stat src_file;
    off_t src_offset = 0;

    if ((src_fd = open(srcpath.c_str(), O_RDONLY)) == -1)
    {
      throw excep::failed_open_file();
    } 
    if ((dest_fd = open(destpath.c_str(), O_CREAT|O_WRONLY|O_TRUNC|O_EXCL, 0660)) == -1)
    {
      close(src_fd);
      throw excep::file_exists();
    }
  fstat(src_fd, &src_file);
  if ((copy_size = sendfile(dest_fd, src_fd, &src_offset, src_file.st_size)) < 0)
    {
      close(src_fd); close(dest_fd);
      throw excep::failed_copy_file();
    }
  else if (copy_size != src_file.st_size) 
    {
      close(src_fd); close(dest_fd);
      throw excep::failed_copy_file();
    }
  
  struct timespec times[2] = { src_file.st_atim, src_file.st_mtim };
  futimens(dest_fd, times);
  fchmod(dest_fd, src_file.st_mode);
  
  close(src_fd); close(dest_fd);
  }

  void filelist::move_file(const std::string & srcpath, const std::string & destpath)
  {
    if (behavior != overwrite_behavior::OVER)
      {
	if ((access(destpath.c_str(), F_OK) != -1))
	  {
	    throw excep::file_exists();
	  }
      }
    
    if (std::rename(srcpath.c_str(), destpath.c_str()) < 0)
      {
	if (errno == EXDEV)
	  {
	    copy_file(srcpath, destpath);
	    remove_file(srcpath);
	    return;
	  }
	throw excep::failed_move_file();
      }
  }

  void filelist::remove_file(const std::string & srcpath)
  {
    if (unlink(srcpath.c_str()) != 0)
      {
	throw excep::failed_to_delete();
      }
  }

  std::string ver_name(const std::string & dest, const std::string & filename)
  {
    const std::string & rootname = filename.substr(0, filename.find_last_of('.'));
    const std::string & extname = get_extname(filename);
    std::string destpath;
    int i = 0;

    do
      {
	++i;

	destpath = cat_path(dest, rootname+" ("+std::to_string(i)+')'+extname);
      }
    while (check_file_exists(destpath));

    return destpath;
  }

  void filelist::move(const decltype(files)::size_type i, const std::string & dest)
  {
    if (files.at(i).removed == true)
      {
	return;
      }
    
    const std::string & filename = files.at(i).name;
    std::string srcpath = cat_path(path, filename);
    std::string destpath = cat_path(dest, filename);

    if (behavior != overwrite_behavior::OVER && check_file_exists(destpath))
      {
	if (behavior == overwrite_behavior::NONE)
	  {
	    std::cout << "File exists\n";
	    return;
	  }
	else if (behavior == overwrite_behavior::RENAME)
	  {
	    std::cout << "file: " << filename << " exists, renaming\n";
	    destpath = ver_name(dest, filename);
	  }
      }
    
    try
      {
	move_file(srcpath, destpath);
      }
    catch (excep::file_error &)
      {
	std::cout << "Failed to move file: " << filename << '\n';
	return;
      }

    files.at(i).removed = true;
    rem++;
    undos.push(op_type::MOVE, &(files.at(i)), srcpath, destpath);
  }

  void filelist::copy(const decltype(files)::size_type i, const std::string & dest)
  {
    if (files.at(i).removed == true)
      {
	return;
      }
    
    const std::string & filename = files.at(i).name;
    std::string srcpath = cat_path(path, filename);
    std::string destpath = cat_path(dest, filename);
    
    if (behavior != overwrite_behavior::OVER && check_file_exists(destpath))
      {
	if (behavior == overwrite_behavior::NONE)
	  {
	    std::cout << "File exists\n";
	    return;
	  }
	else if (behavior == overwrite_behavior::RENAME)
	  {
	    std::cout << "file: " << filename << " exists, renaming\n";
	    destpath = ver_name(dest, filename);
	  }
      }
    
    try
      {
	copy_file(srcpath, destpath);
	std::cout << "file: " << filename << " copied\n";
      }
    catch (excep::file_error &)
      {
	std::cout << "Failed to copy file: " << filename << '\n';
	return;
      }

    undos.push(op_type::COPY, &(files.at(i)), srcpath, destpath);
  }

  void filelist::rename(const decltype(files)::size_type i, const std::string & newname, const bool include_ext)
  {
    if (files.at(i).removed == true)
      {
	return;
      }
    
    const std::string & filename = files.at(i).name;
    std::string srcpath = cat_path(path, filename);
    std::string destname;
    if (include_ext == true)
      {
	destname = newname;
      }
    else
      {
	const std::string & extname = get_extname(filename);
	destname = newname + extname;
      }

    std::string destpath = cat_path(path, destname);

    if (behavior != overwrite_behavior::OVER && check_file_exists(destpath))
      {
	std::cout << "File exists\n";
	return;
      }
    
    try
      {
	move_file(srcpath, destpath);
      }
    catch (excep::file_error &)
      {
	std::cout << "Failed to rename file: " << filename << '\n';
	return;
      }

    undos.push(op_type::RENAME, &(files.at(i)), path, filename);
    files.at(i).name = destname;
  }

  void filelist::remove(const decltype(files)::size_type i)
  {
    if (files.at(i).removed == true)
      {
	return;
      }
    
    std::string srcpath = cat_path(path, files.at(i).name);

    remove_file(srcpath);

    files.at(i).removed = true;
    rem++;
  }
  
  void filelist::multi_move(const decltype(files)::size_type begin, const decltype(files)::size_type end, const std::string & dest)
  {
    undos.push(op_type::MULTI_START);
    
    for (decltype(files)::size_type i = begin; i <= end; ++i)
      {
	move(i, dest);
      }

    undos.push(op_type::MULTI_STOP);
  }

  void filelist::undo_move(op_data & data)
  {
    try
      {
	move_file(data.destpath, data.srcpath);
      }
    catch (excep::file_error &)
      {
	std::cout << "Failed to move file: " << data.entry->name << '\n';
	return;
      }

    rem--;
    data.entry->removed = false;
  }

  void filelist::undo_copy(const op_data & data)
  {
    if (unlink(data.destpath.c_str()) != 0)
      {
	throw excep::failed_to_delete();
      }
  }

  void filelist::undo_rename(op_data & data)
  {
    std::string & path = data.srcpath;
    std::string & origname = data.destpath;
    std::string srcpath = cat_path(path, data.entry->name);
    std::string destpath = cat_path(path, origname);

    try
      {
	move_file(srcpath, destpath);
      }
    catch (excep::file_error &)
      {
	std::cout << "Failed to rename file: " << data.entry->name << '\n';
	return;
      }

    data.entry->name = origname;
  }

  void filelist::undo()
  {
    switch(undos.get_front().type)
      {
      case op_type::MOVE:
	undo_move(undos.get_front());
	std::cout << "Undid moving of file: " << undos.get_front().entry->name << '\n';
	undos.pop();
	break;
      case op_type::COPY:
	undo_copy(undos.get_front());
	std::cout << "Undid copying of file: " << undos.get_front().entry->name << '\n';
	undos.pop();
	break;
      case op_type::RENAME:
        undo_rename(undos.get_front());
	std::cout << "Undid renaming of file: " << undos.get_front().entry->name << '\n';
	undos.pop();
	break;
      case op_type::MULTI_START:
	undos.pop();
	while ((undos.get_front().type != op_type::MULTI_STOP) || (undos.get_front().type != op_type::NONE))
	  {
	    undo();
	  }
	undos.pop();
	break;
      case op_type::MULTI_STOP:
	std::cout << "Errent MULTI_STOP detected.\n";
	break;
      case op_type::NONE:
	break;
      default:
	break;
      }
  }

  int sort_dirs(const std::string & dir_1, const std::string & dir_2)
  {
    return (strverscmp(dir_1.c_str(), dir_2.c_str())) < 0;
  }
  
  dirlist::dirlist(const std::string & p, const std::string & prefix)
  {
    char * hold_path = nullptr;
    if (p.size() > 0)
      {
	hold_path = realpath(p.c_str(), NULL);
      }
    else
      {
	hold_path = realpath("./", NULL);
      }
    if (!hold_path)
      {
	if (errno == ENOENT)
	  {
	    return;
	  }
	throw excep::bad_path();
      }
    path = hold_path;
    if (path.back() != '/')
      {
	path += '/';
      }
    DIR * directory = opendir(hold_path);
    free(hold_path);
    if (!directory)
      {
	throw excep::failed_open_directory();
      }
    struct stat buf;
    for (struct dirent * d = readdir(directory); d; d = readdir(directory))
      {
	std::string dir_name(d->d_name);
	
	if (!strcmp(d->d_name, ".") || !strcmp(d->d_name, ".."))
	  {
	    continue;
	  }

	if (prefix.size() > 0)
	  {
	    std::string::size_type pos = dir_name.find(prefix);
	    if (pos != 0 || pos == dir_name.npos)
	      {
		continue;
	      }
	  }
	    
	if (d->d_type == DT_DIR)
	  {
	    dirs.push_back(d->d_name);
	  }
	else if (d->d_type == DT_LNK)
	  {
	    if(get_stat(path, d->d_name, buf))
	      {
		if (errno != ENOENT)
		  {
		    throw excep::stat_failed();
		  }
		continue;
	      }
	    if (S_ISDIR(buf.st_mode))
	      {
		dirs.push_back(d->d_name);
	      }
	  }
      }
    closedir(directory);
    
    std::sort(dirs.begin(), dirs.end(), sort_dirs);
  }
}
