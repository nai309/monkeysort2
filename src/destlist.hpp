#ifndef DESTLIST_HPP
#define DESTLIST_HPP
#include <vector>
#include <string>
#include <iostream>

#define DEF_DEST_FILE_NAME "dest_list.txt"
#define DEST_MAX 32

namespace excep
{
  class failed_to_open_destfile : public std::exception {};
}

namespace dests
{
  //! Converts relative path to a full path.
  std::string real_path(const std::string & rel_path);
  
  /** @brief Class defining a list of destinations.
   *
   */
  class destlist
  {
  private:
    //! Maximum number of destinations.
    static constexpr int max_dests = 32;

    //! Vector containing destination strings.
    std::vector<std::string> destinations;
    //! Vector containing destination names.
    std::vector<std::string> destinations_display;

    /** @brief Appends new destination to end of destlist.
     *
     * @param dest new destination path
     */
    void push_back(const std::string & dest)
    {
      std::string abs_dest_path = real_path(dest);
      if (abs_dest_path != "")
	{
	  destinations_display.push_back(dest);
	  destinations.push_back(abs_dest_path);
	}
    }
    //! Removes last destination from destlist.
    void pop_back()
    {
      destinations_display.pop_back();
      destinations.pop_back();
    }
    /** @brief Fills destlist with a list of destinations.
     *
     * @param dests vector containing list of relative paths for destinations
     */
    void fill(const std::vector<std::string> & dests)
    {
      int end = (dests.size() > (max_dests-destinations.size())) ?
	(max_dests-destinations.size()) : dests.size();
      for (int i = 0; i < end; ++i)
	{
	  push_back(dests[i]);
	}
    }
    /** @brief Fills the destlist from a file.
     *
     * @param destfile string specifying the relative location of the file
     * listing destinations
     */
    void load(const std::string & destfile);
    
  public:
    destlist() {}
    /** @brief Constructor for \c destlist class.
     *
     * @param dests vector containing list of relative paths for destinations
     */
    explicit destlist(const std::vector<std::string> & dests)
    {
      fill(dests);
    }
    /** @brief Constructor for \c destlist class.
     *
     * @param destfile string specifying the relative loaction of the file
     * listing destinations
     */
    explicit destlist(const std::string & destfile)
    {
      load(destfile);
    }

    /** @brief Appends new destinations to the end of the destlist.
     *
     * @param dests list of new destinations to add
     */
    void append(const std::vector<std::string> & dests)
    {
      fill(dests);
    }
    /** @brief Appends a new destination to the end of the destlist.
     *
     * @param dest relative path to the destination to be added
     */
    void append(const std::string & dest)
    {
      fill({dest});
    }
    /** @brief Changes destination in an existing location in the destlist.
     *
     * @param index position to insert the new destination
     * @param dest new destination path
     */
    void change(const int index, const std::string & dest)
    {
      std::string abs_dest_path = real_path(dest);
      if (abs_dest_path != "")
	{
	  destinations_display.at(index) = dest;
	  destinations.at(index) = abs_dest_path;
	}
    }
    //!Removes the last destination in the destlist.
    void remove()
    {
      if (destinations.size() > 0)
	{
	  pop_back();
	}
    }
    //! Getter for the list of full paths in the destlist.
    const std::vector<std::string> & get_dests()
    {
      return destinations;
    }
    //! Getter for the list of relative paths in the destlist.
    const std::vector<std::string> & get_display_dests()
    {
      return destinations_display;
    }
    //! Getter for length of destlist
    int size()
    {
      return destinations.size();
    }
  };

  /** @brief Class defining a group of destlists.
   *
   */
  class dest_group
  {
  private:
    std::vector<destlist> dest_lists;
    
  public:
    dest_group() {}

    /** @brief Adds a destlist to the dest_group.
     *
     * @param dest_list destlist to add to the dest_group
     */
    void add(const destlist & dest_list)
    {
      dest_lists.push_back(dest_list);
    }
    /** @brief Adds a destlist to the dest_group.
     *
     * @param dests vector of destinations to convert to a destlist and add to the dest_group
     */
    void add(const std::vector<std::string> & dests)
    {
      dest_lists.push_back(destlist(dests));
    }
    /** @brief Adds a destlist to the dest_group.
     *
     * @param destfile string specifying a destination file listing destinations to add
     */
    void add(const std::string & destfile)
    {
      try
	{
	  dest_lists.push_back(destlist(destfile));
	}
      catch (excep::failed_to_open_destfile &)
	{
	  std::cout << "failed to open file: " << destfile << '\n'; 
	  return;
	}
    }
    /** @brief Returns a reference to the destlist at the specified location.
     *
     * @param index position of the requested destlist
     */
    destlist & operator[](const int index)
    {
      return dest_lists.at(index);
    }
    //! Returns the number of destlists in the dest_group
    int size()
    {
      return dest_lists.size();
    }
  };
}

#endif //DESTLIST_HPP
