# Monkeysort2

Monkeysort2 is a specialized image viewer with features included to facilitate the manual organization of images.

## Dependencies

* [Imlib2](https://sourceforge.net/projects/enlightenment/)
* [Windowing](https://gitlab.com/nai309/windowing)

## Installation

### From Source

#### Prerequisites

Install all required dependencies.

#### Build Monkeysort2

```bash
./configure
```

```bash
make
make install
```

## Usage

To use monkeysort2, call it with the folder you wish to pull images from, and any destination folders to sort the images into.
```bash
monkeysort2 [OPTIONS] SRCDIR [DESTDIR(1-32)]
```

## License
[BSD](COPYING)
